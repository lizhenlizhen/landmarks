//
//  ContentView.swift
//  Landmarks
//
//  Created by lizhen on 2021/12/28.
//

// https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/system-icons/

import SwiftUI

struct ContentView: View {
    @State private var selection: Tab = .featured
    
    var body: some View {
        TabView(selection: $selection) {
            CategoryHome()
                .tabItem {
                    Label("Featured",systemImage: "star")
                }
                .tag(Tab.featured)
            LandmarkList()
                .tabItem {
                    Label("list", systemImage: "list.bullet")
                }
                .tag(Tab.list)
        }
    }
    
    enum Tab {
        case featured
        case list
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData())
    }
}
