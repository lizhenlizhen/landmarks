//
//  SimpleView.swift
//  Landmarks
//
//  Created by lizhen on 2021/12/31.
//

import SwiftUI

struct SimpleView: View {
    @Binding var count: Int
    
    var body: some View {
        Text("plus : \(count)")
    }
}

struct SimpleView_Previews: PreviewProvider {
    static var previews: some View {
        SimpleView(count: .constant(0))
    }
}
