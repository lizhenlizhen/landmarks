//
//  TestView.swift
//  Landmarks
//
//  Created by lizhen on 2021/12/31.
//

//https://www.jianshu.com/p/0501f33a02d4

import SwiftUI

struct TestView: View {
    @State var count = 0
    var body: some View {
        List {
            Button(
                action: {
                    count += 1
                },
                label: {
                    Text("plus")
                }
            )
            SimpleView(count: $count)
        }
    }
}

struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}
