//
//  CategoryRow.swift
//  Landmarks
//
//  Created by 远兮 on 2021/12/29.
//

import SwiftUI

struct CategoryRow: View {
    var categoryName: String
    var items: [Landmark]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(categoryName)
                .font(.headline)
                .padding(.leading, 15)
                .padding(.top, 15)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(items) { landmark in
                        NavigationLink {
                            LandmarkDetail(landmark: landmark)
                        } label: {
                            CategoryItem(landmark: landmark)
                        }
                    }
                }
            }
        }
    }
}

struct CategoryRow_Previews: PreviewProvider {
    static var modelData = ModelData()
    
    static var previews: some View {
        CategoryRow(
            categoryName:Landmark.Category.rivers.rawValue,
            items: modelData.categorys[Landmark.Category.rivers.rawValue]!)
    }
}
