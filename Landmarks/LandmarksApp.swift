//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by lizhen on 2021/12/28.
//

// https://www.cnblogs.com/jys509/p/4358950.html

import SwiftUI

@main
struct LandmarksApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
